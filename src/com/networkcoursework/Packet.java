package com.networkcoursework;

import org.w3c.dom.ls.LSOutput;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import static java.util.Arrays.copyOfRange;

public class Packet {

    private  byte[] packetID = new byte[2];
    private byte[] numOfPackets = new byte[2];
    private byte[] checkSum = new byte[2];
    private byte[] zeroField = new byte[2];
    private byte[] msg = new byte[246];
    private byte[] packet = new byte[256];

    public Packet(byte[] msg) {
        this.packet = msg;
        setPacketID(copyOfRange(packet,0, 2));
        setNumOfPackets(copyOfRange(packet,2, 4));
        setCheckSum(copyOfRange(packet,4, 6));
        setZeroField(copyOfRange(packet,6, 10));
        setMsg(copyOfRange(packet,10, 256));
    }

    public void setPacketID(byte[] packetID) {
        this.packetID = packetID;
    }

    public void setNumOfPackets(byte[] numOfPackets) {
        this.numOfPackets = numOfPackets;
    }

    public void setCheckSum(byte[] checkSum) {
        this.checkSum = checkSum;
    }

    public void setZeroField(byte[] zeroField) {
        this.zeroField = zeroField;
    }

    public void setMsg(byte[] msg) {
        this.msg = msg;
    }

    public short getPacketID() {
//        System.out.println(byteToShort(packetID));
        return byteToShort(packetID);
    }

    public short getNumOfPackets() {
//        return numOfPackets;
        return byteToShort(numOfPackets);
    }

    public short getCheckSum() {
//        return ;
        return byteToShort(checkSum);
    }

    public byte[] getZeroField() {
        return zeroField;
    }

    public String getMsg() {
        // return msg;
        return new String (msg,0, byteToShort(checkSum));
    }

    public String getMsgAck() {
        // return msg;
        return new String (msg,0, byteToShort(checkSum));
    }

    public boolean testSendRequest(){

        String s = new String(msg,0, byteToShort(checkSum));
        String y = "011000100111010101101110011001000110000100001010";

        if (s.equals(y)){
            System.out.println("Handshake successful");
            return true;
        }else{
            System.out.println("Handshake unsuccessful");
            return false;
        }

    }

    public boolean reSendRequest(){

        String s = new String(msg,0, byteToShort(checkSum));
        String y = "“011100000110111101100111";

        if (s.equals(y)){
            System.out.println("Re-Send requested");
            return true;
        }else{
            System.out.println("Error : resendrequest");
            return false;
        }

    }

    public boolean testAck(){

        String s = new String(msg,0,byteToShort(checkSum));
        String y = "ACK";

        if (s.equals(y)){
            System.out.println("ACk received and is Successful");
            return true;
        }else{
            System.out.println("ACk Unsuccessful");
            return false;
        }


    }
//    public static int getLength(byte[] arr){
//        int count = 0;
//        for (int x = 0; x < arr.length; x++){
//            if(arr[x] != null){
//                count++;
//            }
//        }
//        return count;
//    }
    // converts byte to short
    public static short byteToShort(byte [] byteBarray){
        return ByteBuffer.wrap(byteBarray).order(ByteOrder.BIG_ENDIAN).getShort();
    }
}
