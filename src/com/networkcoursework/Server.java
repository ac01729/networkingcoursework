package com.networkcoursework;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;

import static java.lang.Thread.sleep;

public class Server {

    // Private Variables
    private DatagramSocket datagramSocket;
    private byte[] buffer = new byte[256];
    private ArrayList<Packet> packetList = new ArrayList<Packet>();
    ;

    // Constructor
    public Server(DatagramSocket datagramSocket) {
        this.datagramSocket = datagramSocket;
    }

    public void respond() {
        while (true) {
            try {
                // receiving packets from client
                buffer = new byte[256];
                DatagramPacket datagramPacket = new DatagramPacket(buffer, buffer.length);
                datagramSocket.receive(datagramPacket);
                Packet testingAckPacket = new Packet(buffer);

                if (testingAckPacket.testSendRequest() == true) {
                    System.out.println("Request to send message, successful sending Ack");
                    try {
                        sendAck(datagramPacket.getAddress(), datagramPacket.getPort());
                        sleep(30);

                        buffer = new byte[256];
                        DatagramPacket recMsgPacket = new DatagramPacket(buffer, 0, buffer.length);
                        datagramSocket.receive(recMsgPacket);
                        Packet packet = new Packet(buffer);
                        System.out.println("Packet Received");
                        packetList.add(0, packet);
                        for (int x = 0; x < packet.getNumOfPackets() - 1; x++) {
//                            Packet packet = new Packet(buffer);
                            datagramSocket.receive(recMsgPacket);
                            packet = new Packet(recMsgPacket.getData());
                            packetList.add(x + 1, packet);
                            System.out.println("Packet Received");
//                                System.out.println(new String (recMsgPacket.getData(),0,256));
                        }

                        // call verify here
                        isPacketVerified(datagramPacket.getAddress(), datagramPacket.getPort());

//                        System.out.println(packet.getMsg());
                        for (int x = 0; x < packet.getNumOfPackets(); x++) {
                            System.out.println(packetList.get(x).getMsg());
                        }


                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);


                    }


//                    if(){

//                    datagramSocket.receive(datagramPacket);
//                    Packet packets = new Packet(buffer);
//                    System.out.println(packets.getMsg());
//                    packetList.add(packets);
//                    packetList.add(packets);
//                    packetList.add(packets);
//
//                    display();
//                    System.out.println("Text received is: " + new String(datagramPacket.getData(), 0, 256));
//                    display(datagramPacket.getData());

//                    }

                } else {
                    System.out.println("Error");
                }


            } catch (IOException e) {
                System.out.println("IOException: " + e.getMessage());
                break;
            }
        }
    }

    // display the message
    public void display() {

    }

    public void decrypt() {

    }

    public void isPacketVerified(InetAddress address, int port) throws IOException {

        // checking if we have all the packets
        int numberOfPackets = packetList.get(0).getNumOfPackets();

        if(numberOfPackets!=packetList.size()){
            System.out.println("Missing Packets");
            int lastPacketId = numberOfPackets - 1;
            ArrayList<Integer> missingPacketIds = new ArrayList<Integer>();
            int ids = 0;
            boolean isThere = false;

            for (int y = 0; y < packetList.size(); y++) {
                if(isThere){
                    ids +=1;
                }else{
                    missingPacketIds.add(y,ids);
                }
                for (int x = ids; x <= lastPacketId; x++) {
                    if (packetList.get(y).getPacketID() == x) {
                        isThere = true;
                        break;
                    }
                    isThere = false;
                }
            }

            if(!isThere){
                System.out.println("Missing Packets");
                requestPacket(missingPacketIds,address, port);
            }
        }

        System.out.println("Received all packets");
        System.out.println("Sending ACK to the client");
        sendAck(address,port);
        // check the packets have the correct checksum
    }


    public void requestPacket(ArrayList<Integer> Ids,InetAddress address, int port) throws IOException {
        System.out.println("Request Packet");
        String s = "011100000110111101100111";
        int sizeOfMsg = s.length();
        byte[] reqBytes = s.getBytes();

        byte[] packet = new byte[256];
        packet = createAckPacket(reqBytes, sizeOfMsg);
        DatagramPacket sendDatagramPacket = new DatagramPacket(packet, packet.length, address, port);
        datagramSocket.send(sendDatagramPacket);

        byte[] receiveData = new byte[256];
        DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
        datagramSocket.receive(receivePacket);

        Packet recPacket = new Packet(receiveData);

        if(recPacket.testAck()){
            System.out.println("Sending required packets to client");
            int[] missingIds = new int[Ids.size()];
            for(int x = 0; x < Ids.size();x++){
                missingIds[x] = Ids.get(x);
            }

            String stringIds = Arrays.toString(missingIds);
            byte[] stringIdsBytes = stringIds.getBytes();
            int sizeStringIds = stringIds.length();
            byte[] stringIdPacket = new byte[256];
            stringIdPacket=createPacket(stringIdsBytes,sizeStringIds);

            DatagramPacket sendStringIdPacket = new DatagramPacket(stringIdPacket, stringIdPacket.length, address, port);
            datagramSocket.send(sendStringIdPacket);



        }



    }

    public void sendAck(InetAddress address, int port) throws IOException {

        String ackString = "ACK";
        byte[] ackBytes = ackString.getBytes();
        int sizeOfString = ackString.length();

        byte[] packet = new byte[256];
        packet = createAckPacket(ackBytes, sizeOfString);
        System.out.println(address);
        DatagramPacket sendDatagramPacket = new DatagramPacket(packet, packet.length, address, port);
        datagramSocket.send(sendDatagramPacket);

    }

    public byte[] createAckPacket(byte[] ackMsg, int sizeOfMsg) throws IOException {
        byte[] packetId = new byte[2];
        byte[] numOfPackets = new byte[2];
        byte[] checksum = new byte[2];
        byte[] zero = new byte[4];
        byte[] packet = new byte[256];

        packetId = shortToByte((short) 0);
        numOfPackets = shortToByte((short) 1);
        checksum = shortToByte((short) sizeOfMsg);
        zero = returnZeroAsByte();
        // concatenation
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        outputStream.write(packetId);
        outputStream.write(numOfPackets);
        outputStream.write(checksum);
        outputStream.write(zero);
        outputStream.write(ackMsg);
        // data packet
        packet = outputStream.toByteArray();
        return packet;
    }

    public byte[] createPacket(byte[] Msg, int sizeOfMsg) throws IOException {
        byte[] packetId = new byte[2];
        byte[] numOfPackets = new byte[2];
        byte[] checksum = new byte[2];
        byte[] zero = new byte[4];
        byte[] packet = new byte[256];

        packetId = shortToByte((short) 0);
        numOfPackets = shortToByte((short) 1);
        checksum = shortToByte((short) sizeOfMsg);
        zero = returnZeroAsByte();
        // concatenation
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        outputStream.write(packetId);
        outputStream.write(numOfPackets);
        outputStream.write(checksum);
        outputStream.write(zero);
        outputStream.write(Msg);
        // data packet
        packet = outputStream.toByteArray();
        return packet;
    }


    // converts int to byte
    public static byte[] shortToByte(short myInteger) {
        return ByteBuffer.allocate(2).order(ByteOrder.BIG_ENDIAN).putShort(myInteger).array();
    }

    public static byte[] returnZeroAsByte() {
        int myInteger = 0;
        return ByteBuffer.allocate(4).order(ByteOrder.BIG_ENDIAN).putInt(myInteger).array();
    }

    public static void main(String[] args) throws SocketException {
        DatagramSocket socket = new DatagramSocket(5000);
        Server server = new Server(socket);
        server.respond();
    }


}


// useful code noted out for receiving end

//         String returnString = new String(datagramPacket.getData(), 0, datagramPacket.getLength());
//         buffer = returnString.getBytes();
//         InetAddress address = datagramPacket.getAddress();
//         int port = datagramPacket.getPort();
//         datagramPacket = new DatagramPacket(buffer, buffer.length, address, port);
//         datagramSocket.send(datagramPacket);