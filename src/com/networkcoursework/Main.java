package com.networkcoursework;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {
        // setting up client side server
        DatagramSocket datagramSocket = new DatagramSocket();
        Scanner scanner = new Scanner(System.in);
        //InetAddress address = InetAddress.getByName("10.77.45.115");
        int index = 0;
        while(index != 9000){
//            InetAddress address = InetAddress.getLocalHost();
            System.out.println("Welcome to the text msg service");
            System.out.println("set send address");
            String readString = scanner.nextLine();
            InetAddress address = InetAddress.getByName(readString);

            scanner = new Scanner(System.in);
            System.out.println("set send port number");
            int port = scanner.nextInt();

            scanner = new Scanner(System.in);
            System.out.println("send message");
            String msg = scanner.nextLine();

            Client client = new Client(datagramSocket, address, port);
            client.send(msg);

        }





    }
}
