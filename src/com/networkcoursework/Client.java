package com.networkcoursework;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Scanner;

public class Client {

    private final DatagramSocket datagramSocket;
    private InetAddress address;
    private byte[] buffer;
    private int port;
    private ArrayList <Packet> packetList = new ArrayList<Packet>();


    public Client(DatagramSocket datagramSocket, InetAddress address, int port) {
        this.datagramSocket = datagramSocket;
        this.address = address;
        this.port = port;
    }

    public void send(String msg) throws IOException {

        // collecting text message

        boolean isConnected = handshake();
        if(isConnected == true){

            System.out.println("ACK received from request to send message, successful");

            int sizeOfMsg = msg.length();
//            System.out.println(msg);
            buffer = msg.getBytes();
            partitionMsg(buffer);

            datagramSocket.setSoTimeout(50);

            byte[] receiveData = new byte[256];
            DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
            datagramSocket.receive(receivePacket);
            Packet packet = new Packet(receiveData);
            // check if its request/ or ack packet
            if(packet.testAck()){

                System.out.println("");

            }else if(packet.reSendRequest()){

                // resending packets

                sendAck();
                byte[] recStringIdPacket = new byte[256];
                DatagramPacket receiveStringIdPacket = new DatagramPacket(recStringIdPacket, recStringIdPacket.length);
                datagramSocket.receive(receiveStringIdPacket);



            }


        }else{

            for(int x = 0; x < 3; x++){
                System.out.println("Attempting to reconnect to the client.. ");
                boolean attempts = handshake();
                if(!attempts){
                    System.out.println("Handshake Failed");
                }
            }

        }




    }

    // partitioning the msg
    // message = 246 bytes
    // header = 10 bytes
    public Byte partitionMsg(byte msg[]) throws IOException {

        // store the message
//        byte msg = new byte
        int lengthOfMsg = msg.length;
//        System.out.println(lengthOfMsg);
        int partitionIndex = 0;
        int msgIndex = 0;
        byte[] packetBufferParts;

        byte[] packetId = new byte[2];
        byte[] numOfPackets = new byte[2];
        byte[] checksum = new byte[2];
        byte[] zero = new byte[4];
        byte[] packet = new byte[256];
        boolean isPartitionIndexDecimal = false;


        if (lengthOfMsg > 246) {

            // setting up the partition index
            if (isEvenlyDivisable(lengthOfMsg, (lengthOfMsg / 246))) {
                partitionIndex = lengthOfMsg / 246;
                isPartitionIndexDecimal = false;
            } else {
                partitionIndex = (lengthOfMsg / 246) + 1;
                isPartitionIndexDecimal = true;
            }

            // looping the msg array, and partioning it up
            msgIndex = 0;
            int msgLimitIndex = 246;
            // re-initializing the byte arrays
            packetId = new byte[2];
            numOfPackets = new byte[2];
            checksum = new byte[2];
            zero = new byte[4];
            packet = new byte[256];
            packetBufferParts = new byte[246];

            // checksum array edit
            int[] checkArr;

            for (int i = 0; i < partitionIndex; i++) {

//                System.out.println(i);

                if (i == (partitionIndex - 1)) {
                    if (isPartitionIndexDecimal) {
                        msgLimitIndex = lengthOfMsg;
                        packetBufferParts = new byte[246];
                    }
                }

                int x = 0;

                for (int j = msgIndex; j < msgLimitIndex; j++) {
                    packetBufferParts[x] = msg[j];
                    x++;
                }

                // content for header
                packetId = shortToByte((short) i);
                numOfPackets = shortToByte((short) partitionIndex);

                if(isPartitionIndexDecimal && i == (partitionIndex - 1)){
                    int diff = lengthOfMsg - msgIndex;
                    checksum = shortToByte((short) diff);
                }else {
                    checksum = shortToByte((short) packetBufferParts.length);
                }

                zero = returnZeroAsByte();
                // concatenation
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                outputStream.write(packetId);
                outputStream.write(numOfPackets);
                outputStream.write(checksum);
                outputStream.write(zero);
                outputStream.write(packetBufferParts);
                // data packet
                packet = outputStream.toByteArray();
                Packet packets = new Packet(packet);
                packetList.add(i,packets);
                // sending the packets
                DatagramPacket datagramPacket = new DatagramPacket(packet, packet.length, address, port);
                datagramSocket.send(datagramPacket);

//                System.out.println(msgIndex);
//                System.out.println(msgLimitIndex);

                msgIndex += 246;
                msgLimitIndex += 246;
//                System.out.println(new String(packetBufferParts, 0, 246));
            }

        } else {

            // content for header
            packetId = shortToByte((short) 0);
            numOfPackets = shortToByte((short) 1);
            checksum = shortToByte((short) lengthOfMsg);
            zero = returnZeroAsByte();
            // concatenation
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            outputStream.write(packetId);
            outputStream.write(numOfPackets);
            outputStream.write(checksum);
            outputStream.write(zero);
            outputStream.write(msg);
            // data packet
            packet = outputStream.toByteArray();
            Packet packets = new Packet(packet);
            packetList.add(0,packets);
            // sending the packets
            DatagramPacket datagramPacket = new DatagramPacket(packet, packet.length, address, port);
            datagramSocket.send(datagramPacket);

        }


        return null;
    }

    // Making the Header
    public void header() {

    }

    public boolean handshake() throws IOException {


            System.out.println("Handshake Send");
            String handshakeString = "011000100111010101101110011001000110000100001010";
            byte[] handshakeBytes = handshakeString.getBytes();
            byte[] packet = new byte[256];
            byte[] receiveData = new byte[256];

            datagramSocket.setSoTimeout(5);
            try{

                packet = createHandshakePacket(handshakeBytes, handshakeString.length());
                DatagramPacket datagramPacket = new DatagramPacket(packet, packet.length, address, port);
                datagramSocket.send(datagramPacket);

                // receiving conformation, that the server received the message

                DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                datagramSocket.receive(receivePacket);
                Packet verifyAckPacket = new Packet(receiveData);
                boolean verifyAck = verifyAckPacket.testAck();
                return verifyAck;


            }catch (SocketTimeoutException e ){
                return false;

            }





//            String messageFromServer = new String(receivePacket.getData(), 0, receivePacket.getLength());
//            System.out.println("Text received is: " + messageFromServer);


    }

    public void encrypt() {

    }

    public byte[] createHandshakePacket(byte[] handshakeMsg, int checksumLength) throws IOException {
        byte[] packetId = new byte[2];
        byte[] numOfPackets = new byte[2];
        byte[] checksum = new byte[2];
        byte[] zero = new byte[4];
        byte[] packet = new byte[256];

        packetId = shortToByte((short) 0);
        numOfPackets = shortToByte((short) 1);
        checksum = shortToByte((short) checksumLength);
        zero = returnZeroAsByte();
        // concatenation
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        outputStream.write(packetId);
        outputStream.write(numOfPackets);
        outputStream.write(checksum);
        outputStream.write(zero);
        outputStream.write(handshakeMsg);
        // data packet
        packet = outputStream.toByteArray();
        return packet;
    }

    public void sendAck() throws IOException {

        String ackString = "ACK";
        byte[] ackBytes = ackString.getBytes();
        int sizeOfString = ackString.length();

        byte[] packet = new byte[256];
        packet = createAckPacket(ackBytes, sizeOfString);
        System.out.println(address);
        DatagramPacket sendDatagramPacket = new DatagramPacket(packet, packet.length, address, port);
        datagramSocket.send(sendDatagramPacket);

    }

    public byte[] createAckPacket(byte[] ackMsg, int sizeOfMsg) throws IOException {
        byte[] packetId = new byte[2];
        byte[] numOfPackets = new byte[2];
        byte[] checksum = new byte[2];
        byte[] zero = new byte[4];
        byte[] packet = new byte[256];

        packetId = shortToByte((short) 0);
        numOfPackets = shortToByte((short) 1);
        checksum = shortToByte((short) sizeOfMsg);
        zero = returnZeroAsByte();
        // concatenation
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        outputStream.write(packetId);
        outputStream.write(numOfPackets);
        outputStream.write(checksum);
        outputStream.write(zero);
        outputStream.write(ackMsg);
        // data packet
        packet = outputStream.toByteArray();
        return packet;
    }
    public boolean isEvenlyDivisable(int a, int b) {
        return a % b == 0;
    }

    // converts int to byte
    public static byte[] shortToByte(short myInteger) {
        return ByteBuffer.allocate(2).order(ByteOrder.BIG_ENDIAN).putShort(myInteger).array();
    }

    // returns 0 for the header

    public static byte[] returnZeroAsByte() {
        int myInteger = 0;
        return ByteBuffer.allocate(4).order(ByteOrder.BIG_ENDIAN).putInt(myInteger).array();
    }

    // converts byte to short
    public static short byteToShort(byte[] byteBarray) {
        return ByteBuffer.wrap(byteBarray).order(ByteOrder.BIG_ENDIAN).getShort();
    }


//    public static void main(String[] args) throws SocketException, UnknownHostException {
//        // setting up client side server
//        DatagramSocket datagramSocket = new DatagramSocket();
//        //InetAddress address = InetAddress.getByName("10.77.45.115");
//
//        System.out.println("Welcome to the text msg service");
//        System.out.println("");
//
//        InetAddress address = InetAddress.getLocalHost();
//        Client client = new Client(datagramSocket, address, port);
//        client.send();
//
//
//    }
}
